import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { PdfServiceService } from './pdf-service.service';
import { HttpClient } from '@angular/common/http';
declare const myfun: any
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public status: boolean = false

  constructor(
    private pdfServiceService: PdfServiceService,
    private http: HttpClient) {
  }

  ngOnInit(): void { }

  receiveMessage($event) {
    this.status = $event
  }

}
