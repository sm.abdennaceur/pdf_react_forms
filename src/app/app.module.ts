import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AngularDraggableModule } from 'angular2-draggable';
import { LoadPdfComponent } from './load-pdf/load-pdf.component';
import { FluidHeightDirective } from './fluid-height.directive';
import { SignaturePadModule } from 'angular2-signaturepad';
import { HttpClientModule } from '@angular/common/http';
import { SignatureModalComponent } from './signature-modal/signature-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
@NgModule({
  declarations: [
    AppComponent,
    DragDropComponent,
    LoadPdfComponent,
    FluidHeightDirective,
    SignatureModalComponent
  ],
  imports: [
    PDFExportModule,
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    PdfViewerModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularDraggableModule,
    DragDropModule,
    MatButtonModule,
    MatCheckboxModule,
    SignaturePadModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
