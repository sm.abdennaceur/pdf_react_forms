import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
 
import SignaturePad from 'signature_pad'
@Component({
  selector: 'app-signature-modal',
  template: `
           <!-- <div class="modal-header">
                <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="signature-pad">
             <div class="signature-pad--body">
              <canvas #sPad width="167" height="81" style="touch-action: none;"></canvas>
              </div>
            </div>
            <div class="modal-footer">
              <div class="row">
                <div class="col-4">
                  <button class="btn btn-block btn-danger" (click)="clear()">Clear</button>
                </div>
                <div class="col-4">
                <button class="btn btn-block btn-success" (click)="save();activeModal.close('Close click')">PNG</button>
                </div>
                <div class="col-4">
                <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
                </div>
              </div>
            </div>`,
 
})
export class SignatureModalComponent implements AfterViewInit {

  @Output() EndOfsignature = new EventEmitter<string>();
  @ViewChild('sPad', { static: true }) signaturePadElement;
  signaturePad: any;

  constructor(public activeModal: NgbActiveModal,) { }

  ngAfterViewInit(): void {
    this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
  }

  clear() {
    this.signaturePad.clear()
  }

  save() {
    if (this.signaturePad.isEmpty()) {
      alert('Please provide a signature first.');
    } else {
      let base64Image = this.signaturePad.toDataURL();
      this.sendEndsignature(base64Image)
      // this.download(base64Image, 'signature.png');
    }
  }

  download(dataURL, filename) {
    if (navigator.userAgent.indexOf('Safari') > -1 && navigator.userAgent.indexOf('Chrome') === -1) {
      window.open(dataURL);
    } else {
      const blob = this.dataURLToBlob(dataURL);
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    }
  }

  dataURLToBlob(dataURL) {
    // Code taken from https://github.com/ebidel/filer.js
    const parts = dataURL.split(';base64,');
    const contentType = parts[0].split(':')[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);
    for (let i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }
    return new Blob([uInt8Array], { type: contentType });
  }

  sendEndsignature(image:string) {
    this.EndOfsignature.emit(image);
  }
}
