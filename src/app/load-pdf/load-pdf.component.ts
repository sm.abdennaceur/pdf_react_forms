
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Input } from '../input';
import { SignaturePad } from 'angular2-signaturepad';
import { PdfServiceService } from '../pdf-service.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SignatureModalComponent } from '../signature-modal/signature-modal.component';
@Component({
  selector: 'app-load-pdf',
  templateUrl: './load-pdf.component.html',
  styleUrls: ['./load-pdf.component.scss']
})
export class LoadPdfComponent implements OnInit, AfterViewInit {
  data: SafeHtml;
  pdfPosstion: any;
  public scale = 0.8;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  @ViewChild('cmpContainer') cmpContainer: ElementRef;


  private signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 5,
    'canvasWidth': 500,
    'canvasHeight': 300
  };


  readonly dpiRatio = 96 / 72;
  public pdfSrc = '/assets/test.pdf';
  public myForm: FormGroup;
  imageUrl: any
  showSigbtn = false
  modalRef
  public inputList: Input[] = [];

  public zoom = 1;

  constructor(private _fb: FormBuilder,
    private pdfServiceService: PdfServiceService,
    private sanitizer: DomSanitizer,
    private changeDetector: ChangeDetectorRef,
    private modalService: NgbModal,) {
    this.myForm = this._fb.group({});
  }
  ngAfterViewInit(): void {
    // this.signaturePad.set('minWidth', 5);

  }
  ngOnInit(): void {
    this.pdfServiceService.getPDF2HTml().subscribe(data => {
      this.data = this.sanitizer.bypassSecurityTrustHtml(data)
      this.changeDetector.detectChanges();
      
      let HTMLCollection = this.cmpContainer.nativeElement.getElementsByTagName("svg")[0]
      // this.cmpContainer.nativeElement.getElementsByClassName("page");
      console.log('this.HTMLCollection ',HTMLCollection)
      this.pdfPosstion = this.pdfServiceService.calcTopOffset(HTMLCollection)
      console.log('this.pdfPosstion ',this.pdfPosstion)
    }).add(() => {
      this.loadComplete()
      this.signaturePad.clear();
    })

  }
  private createInput(annotation: PDFAnnotationData, rect: number[] = null) {
    let formControl = new FormControl(annotation.buttonValue || '');

    const input = new Input();
    input.name = annotation.fieldName;

    if (annotation.fieldType === 'text') {
      input.type = 'text';
      input.value = annotation.buttonValue || '';
    }
    if (annotation.fieldType === 'date') {
      input.type = 'date';
      input.value = annotation.buttonValue || '';
    }
    if (annotation.fieldType === 'time') {
      input.type = 'time';
      input.value = annotation.buttonValue || '';
    }
    if (annotation.fieldType === 'Btn' && !annotation.checkBox) {
      input.type = 'radio';
      input.value = annotation.buttonValue;
    }

    if (annotation.checkBox) {
      input.type = 'checkbox';
      input.value = true;
      formControl = new FormControl(annotation.buttonValue || false);
    }
    if (annotation.fieldType == 'checkbox') {
      input.type = 'checkbox';
      input.value = true;
      formControl = new FormControl(annotation.buttonValue || false);
    }
    if (annotation.fieldType == 'signature') {
      input.type = 'signature';
      input.value = 'signatureValue';
      formControl = new FormControl(annotation.buttonValue || false);
    }
    // Calculate all the positions and sizes
    if (rect) {
      input.top = rect[0]
      input.left = rect[1]
    }

    this.inputList.push(input);
    return formControl;
  }

  receiveEndSignature($event) {
    this.showSigbtn = true
    this.imageUrl = $event
    console.log('this.imageUrl :', this.imageUrl)
    this.transform()
  }
  transform() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.imageUrl);
  }
  private addInput(annotation: PDFAnnotationData, rect: number[] = null): void {
    this.myForm.addControl(annotation.fieldName, this.createInput(annotation, rect));
  }

  public getInputPosition(input: Input): any {
    return {
      top: `${input.top + this.pdfPosstion.top}px`,
      left: `${input.left + this.pdfPosstion.left}px`,
      // height: `${input.height}px`,
      // width: `${input.width}px`,
    };
  }
  public getInputPositionByName(inputName: string): any {
    console.log('this.inputsList',this.inputList)
    let input = this.inputList.find(input=>input.name === inputName)
    return {
      top: `${input.top + this.pdfPosstion.top}px`,
      left: `${input.left + this.pdfPosstion.left}px`,
      // height: `${input.height}px`,
      // width: `${input.width}px`,
    };
  }
  open() {
   const modalRef = this.modalService.open(SignatureModalComponent);
   modalRef.componentInstance.EndOfsignature.subscribe((receivedEntry) => {
    console.log(receivedEntry);
    this.showSigbtn = true
    this.imageUrl = receivedEntry
    
    this.transform()
    })
  }
  public loadComplete(): void {
    let Inputs = JSON.parse(localStorage.getItem('elements'))
    if (Inputs) {
      Inputs.forEach(element => {
        this.addInput(element, element.postion);
      });
    }
  }
}
