import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
@Injectable({
  providedIn: 'root'
})
export class PdfServiceService {

  constructor(private http: HttpClient) { }


  getPDF2HTml(){
    const headers: HttpHeaders = new HttpHeaders({'Accept': 'text/html'});
    return this.http.get('http://localhost:3000/wajdi', { headers: headers, responseType: 'text' }) 
  }
  calcTopOffset(domElement): any {
    try {
      const rect = domElement.getBoundingClientRect();
      const scrollTop =
        window.pageYOffset || document.documentElement.scrollTop;
     return { top: (rect.top + scrollTop), left: rect.left };
    } catch (e) {
      return 0;
    }
  }
}
