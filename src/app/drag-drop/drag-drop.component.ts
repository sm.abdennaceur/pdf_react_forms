 
import { Component, ElementRef, OnInit, Output, ViewChild,EventEmitter, AfterViewInit, HostListener,  ChangeDetectorRef } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { SignaturePad } from 'angular2-signaturepad';
import { PdfServiceService } from '../pdf-service.service';
 
@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.scss']
})
export class DragDropComponent implements OnInit,AfterViewInit {
  public typeUser: boolean;
  public collapsed: boolean;
  public userData: any;
  public inBounds: boolean;
  public edge: any;
  public pdfViewerConfig: any;
  public elementsDragAndDrop: any[];
  public listesElementInTheDoc: any[];
  public todayDate: string;
  public signatureValue: any;
  public pdfPosstion:any
  htmlCollection:HTMLCollection
  @ViewChild('div') div: ElementRef ;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;


  @Output() EndDragAndDrop = new EventEmitter<boolean>();
  
  @ViewChild('cmpContainer') cmpContainer: ElementRef;

  @HostListener('window:resize', ['$event'])
  
  private signaturePadOptions: Object = {
    minWidth: 5,
    canvasWidth: 150,
    canvasHeight: 100
  };

  data: SafeHtml;

  constructor(
    private pdfServiceService:PdfServiceService,
    private sanitizer: DomSanitizer,
    private changeDetector: ChangeDetectorRef) {   
    this.onResize();   
    localStorage.clear();
  }

  ngAfterViewInit(): void {
  }
 
  onResize(event?) {
      
  }
  ngOnInit() {
     this.pdfServiceService.getPDF2HTml().subscribe(data=>{
       this.data = this.sanitizer.bypassSecurityTrustHtml(data);
       this.changeDetector.detectChanges();
       
       this.htmlCollection = this.cmpContainer.nativeElement.getElementsByTagName("svg")[0];
       console.log('htmlCollection :',this.htmlCollection)
       this.pdfPosstion = this.pdfServiceService.calcTopOffset(this.htmlCollection)
       console.log('this.pdfPosstion',this.pdfPosstion)
     })
    
    
    this.signatureValue = '';
    this.todayDate = new Date().toLocaleDateString();
    this.typeUser = false;
    this.collapsed = true;
    this.inBounds = true;
   
    this.edge = {
      top: true,
      bottom: true,
      left: true,
      right: true
    };
    this.pdfViewerConfig = {
      zoom: 1.0,
      rotation: 0,
      showAll: true,
      src: '/assets/test.pdf'
      // src:"/assets/Chamekh_ala_eddine_cv.pdf",
    };

   

    this.elementsDragAndDrop = [
      {
        id: this.getRandomId(),
        name: 'date',
        text: 'Texte Libre',
        fieldType: 'date',
        colorClass: 'bg-yellow',
        parent: true,
        postion:[],
        fieldName:'CreationDate'
      },
      {
        id: this.getRandomId(),
        name: 'Heure de début',
        text: 'Texte Libre',
        fieldType: 'time',
        colorClass: 'bg-yellow',
        parent: true,
        postion:[],
        fieldName:'heuredebut'
      },
      {
        id: this.getRandomId(),
        name: 'type',
        fieldType: 'text',
        text: 'Date du Jour',
        colorClass: 'bg-red',
        parent: true,
        postion:[],
        fieldName:'type'
      },
      {
        id: this.getRandomId(),
        name: 'Montant',
        fieldType: 'text',
        text: 'Case à Cocher',
        colorClass: 'bg-red',
        parent: true,
        postion:[],
        fieldName:'Montant'
      },
      {
        id: this.getRandomId(),
        name: 'Non',
        fieldType: 'text',
        text: 'Case à Cocher 2',
        colorClass: 'bg-red',
        parent: true,
        postion:[],
        fieldName:'Nom'
      },
      {
        id: this.getRandomId(),
        name: 'Numéro de code',
        fieldType: 'text',
        text: 'Zone de Saisie Libre',
        colorClass: 'bg-red',
        parent: true,
        postion:[],
        fieldName:'Numerocode'
      },
      {
        id: this.getRandomId(),
        name: 'signature',
        fieldType: 'signature',
        text: 'Signature',
        colorClass: 'bg-green',
        parent: true,
        postion:[],
        fieldName:'signature'
      }
    ];
    this.listesElementInTheDoc = [];
  }

  drawComplete() {
    //this.signatureValue = this.signaturePad.toDataURL();
  }

  drawStart() {
    console.log('begin drawing');
  }
  getRandomId() {
    return Math.floor(Math.random() * Math.floor(1000));
  }
  addElementToDrag(element) {
    this.listesElementInTheDoc.push(element);
    let index= this.listesElementInTheDoc.find(elm=> elm.id === element.id)
    if(index !== -1){
      this.elementsDragAndDrop.splice(index,1)
    }
    this.changeDetector.detectChanges();
    this.pdfPosstion = this.pdfServiceService.calcTopOffset(this.htmlCollection) 
    
  }
  incrementZoom(amount: number) {
    this.pdfViewerConfig.zoom += amount;
  }
  rotate(angle: number) {
    this.pdfViewerConfig.rotation += angle;
  }
  
  toggleAccordion() {
    this.collapsed = !this.collapsed;
  }
  // onSubmitSaveSignatureModel(form: NgForm) {
  //   if (form.valid) {
  //     console.log(form.value);
  //   } else {
  //     console.log('probleme form ... ');
  //   }
  // }

  checkEdge(event) {
    this.edge = event;
  }
  onStart(event) {
    // console.log(event);
  }

  onStop(event, formElm) {

  }
   
  onMoving(event) {
   
  }

  onMoveEnd(event,formElm,myHandle) {
   let elementPostion = this.pdfServiceService.calcTopOffset(myHandle)
   let elem= this.listesElementInTheDoc.find(element=> element.id === formElm.id)
   elem.postion = []
   elem.postion.push(elementPostion.top-this.pdfPosstion.top,elementPostion.left-this.pdfPosstion.left)
  }
  onSave(listesElementInTheDoc:any[]){
    localStorage.setItem('elements', JSON.stringify(listesElementInTheDoc));
    this.sendMessage(true)
  }
  removeDragEement(element) {
    const index = this.listesElementInTheDoc.findIndex(
      x => x.id === element.id
    );
    this.listesElementInTheDoc.splice(index, 1);
  }
  sendMessage(status:boolean) {
    this.EndDragAndDrop.emit(status);
  }

  
  
}
