import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { LoadPdfComponent } from './load-pdf/load-pdf.component';

const routes: Routes = [
  { path: '', component: DragDropComponent },
 { path: 'pdforms', component: LoadPdfComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
